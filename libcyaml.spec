Summary: C library for reading and writing YAML
Name: libcyaml
Version: 0.1
Release: 2%{?dist}
License: ISC
URL: https://gitlab.com/alextee/libcyaml
Source0: https://gitlab.com/alextee/libcyaml/-/archive/0.1/libcyaml-0.1.tar.gz
Group: Applications/Multimedia
Packager: Alexandros Theodotou

BuildRequires: libyaml-devel
BuildRequires: gcc
BuildRequires: pkgconfig
Requires: glibc
Requires: libyaml

BuildRoot: ~/rpmbuild/

%description
LibCYAML is a C library for reading and writing structured YAML documents.
It is written in ISO C11 and licensed under the ISC licence.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The %{name}-devel package contains header files for %{name}.

%prep
%setup -q

%build
%{__make} %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}
mkdir -p %{buildroot}%{_libdir}/pkgconfig %{buildroot}%{_includedir}
%{__make} install DESTDIR=%{buildroot} PREFIX=/usr LIBDIR=lib64

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/*

%files devel
%{_includedir}/*

%changelog
* Tue Jan 22 2019 Alexandros Theodotou <alex at zrythm dot org> 0.1
- RPM release
