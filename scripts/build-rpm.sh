#!/bin/sh
mkdir -p $_PATH
cd
curl -O https://gitlab.com/alextee/$CI_PROJECT_NAME/-/archive/$CI_COMMIT_REF_NAME/$_PACKAGE_NAME.tar.gz
tar xf $_PACKAGE_NAME.tar.gz
cp $_PACKAGE_NAME/$CI_PROJECT_NAME.spec rpmbuild/SPECS/
cp $_PACKAGE_NAME.tar.gz rpmbuild/SOURCES/
cd rpmbuild/SPECS/
rpmbuild -ba $CI_PROJECT_NAME.spec
