#!/bin/sh
mkdir -p $_PATH
cd
PACKAGE_NAME="$CI_PROJECT_NAME"_"$CI_COMMIT_REF_NAME"
curl -O https://gitlab.com/alextee/$CI_PROJECT_NAME/-/archive/$CI_COMMIT_REF_NAME/$_PACKAGE_NAME.tar.gz
cp $_PACKAGE_NAME.tar.gz $_PATH/$PACKAGE_NAME.orig.tar.gz
tar xf $_PACKAGE_NAME.tar.gz --directory $_PATH/
cd $_PATH/$_PACKAGE_NAME
debuild -us -uc

